import React, { useEffect, useState,} from 'react';
import { Link } from 'react-router-dom';

function VehicleColumn(props){
    return(
        <div className="col-sm-6 my-5">
            {props.list.map(data => {
                const model = data;
                return(
                <div key ={model.id}class="card mb-3 shadow w-75 p-3">
                <img src={model.picture_url} class="card-img-top" alt=""/>
                <div class="card-body">
                <h5 class="card-title">{model.name}</h5>
                <p class="card-text">{model.manufacturer.name}</p>
                <Link to="/automobiles/list" type="button" className="btn btn-primary btn-sm">Go</Link>
                {/* <a href="#" class="btn btn-primary">See Automobiles</a> */}
                </div>
                </div>
        
                );
            })}
        </div>
    );
}



function VehicleModelList(props){
    // const [models, setModels] = useState([]);
    const [modelColumns, setModelColumns] = useState([[], [], []]);


    const fetchVehicles = async () => {
        const url = 'http://localhost:8100/api/models/';

        try{
            const response = await fetch(url);
            if (response.ok){
                const data = await response.json();
                const requests = [];
                for (let model of data.models){ 
                    const detailUrl = `http://localhost:8100${model.href}`;
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const columns = [[],[],[]];
                let i = 0;
                for(const modelResponse of responses){
                    if(modelResponse.ok){
                        const details = await modelResponse.json();
                        columns[i].push(details);
                        i=i+1;
                        if(i > 2){
                            i = 0;
                        }
                    } else {
                        console.error(modelResponse);
                    }
                }
                setModelColumns(columns);
        }
        } catch (e) {
            console.error(e);
        }
    }
    useEffect(()=> {
        fetchVehicles();
    }, []);

    // async function loadVehicles() {
    //     const response = await fetch('http://localhost:8100/api/models/');
    //     if (response.ok) {
    //         const data = await response.json();
    //         setModels(data.models)
    //     }
    //     }
    //     useEffect(() => {loadVehicles();}, []);

    return(
        <div className="container-fluid my-5 py-5">
            <div>
                <h1 className='text-center'>Vehicle Models</h1>
            </div>
            <div className="row">
                {modelColumns.map((modelList, index )=> {
                        return(
                        <VehicleColumn key={index} list={modelList} />
                        );
                        })}
            {/* <table className = "table table-striped">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return(
                            <tr key ={model.id} > 
                            <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url}/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table> */}
        </div>
        </div>
    )
}

export default VehicleModelList;
