import React, {useState,} from 'react';

function CustomerForm(){
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data={};

        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

    const customersUrl = 'http://localhost:8090/api/customers/';

    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(customersUrl, fetchConfig);
    if (response.ok) {
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhoneNumber('');
    }
    }

    const handleFirstNameChange = (event)=> {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event)=> {
        const value = event.target.value;
        setLastName(value);
    }

    const handleAddressChange = (event)=> {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumberChange = (event)=> {
        const value = event.target.value;
        setPhoneNumber(value);
    }
    
    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add A Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFirstNameChange}placeholder="first_name" required type="text" id="first_name" name="first_name" value={firstName} className="form-control"/>
                    <label htmlFor="name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleLastNameChange} placeholder="last_name" required type="text" id="last_name" name="last_name" value={lastName} className="form-control"/>
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleAddressChange} placeholder="Address" required type="text" name="address" value={address} id="address" className="form-control"/>
                    <label htmlFor="address">Address</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="phone_number">Phone Number</label>
                    <input onChange={handlePhoneNumberChange} placeholder="Phone Number" type="text" name="phone_number" value={phoneNumber} id="phone_number" className="form-control"/>
                </div>
                <button className="btn btn-primary me-md-4">Create</button>
            </form>
            </div>
            </div>
        </div>
    )
}

export default CustomerForm;
