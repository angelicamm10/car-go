import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg fixed-top navbar-dark" style ={{backgroundColor: 'blue'}}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarGo</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
              <ul className="navbar-nav">            
                <li className="nav-item dropdown">
                  <NavLink className="nav-link active dropdown-toggle" aria-current="page" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" to="/models/">Inventory</NavLink>
                  <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><NavLink className="dropdown-item" aria-current="page" to="/models/list">Vehicle Models</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/models/new">Add a Vehicle Model</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/automobiles/list">Automobiles List</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/automobiles/new">Add an Automobile</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/manufacturers/list">Manufacturers</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new">Add a Manufacturer</NavLink></li>
                    <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      
                    </ul>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link active dropdown-toggle" aria-current="page" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" to="/salesperson/">Sales</NavLink>
                  <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><NavLink className="dropdown-item" aria-current="page" to="/salesperson/list">Salespeople List</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/salesperson/new">Add a Salesperson</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/salesperson/history">Salesperson History</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/sales/list">Sales List</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/sales/new">Record a Sale</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/customers/list">Customers List</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/customers/new">Add a Customer</NavLink></li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link active dropdown-toggle" aria-current="page" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" to="/technicians/list">Services</NavLink>
                  <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><NavLink className="dropdown-item" aria-current="page" to="/appointments/list">Appointments</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/appointments/new">Request an Appointment</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/appointments/history">Appointment History</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/technicians/list">Technicians List</NavLink></li>
                    <li><NavLink className="dropdown-item" aria-current="page" to="/technicians/new">Add a Technician</NavLink></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
  </nav>
  )
}

export default Nav;
