import React, { useState ,useEffect } from 'react'
function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    const vinCheck = async (vin) => {
      const url = 'http://localhost:8080/api/vin/'
      const fetchConfig = {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok) {
        const data = await response.json()
        const autos = data.autos
        return autos.some((auto) => vin === auto.vin)
      }
      return false
    }


    const fetchData = async () => {
      const url = 'http://localhost:8080/api/appointments/'
      const response = await fetch(url)
      if (response.ok) {
          const data = await response.json()
          const appointments = data.appointments
          for (const appointment of appointments) {
            if (appointment.status.name === 'sub') {
              const vip = await vinCheck(appointment.vin)
              appointment.isVIP = vip
            }
          }
          setAppointments(appointments)
      }
    }


    const handleFinish = async (appointment) => {
      const url = `http://localhost:8080/api/appointments/${appointment}/finish/`
      const fetchConfig = {
        method: "put",
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok){
        window.location.reload()
      }
    }


    const handleCancel = async (appointment) => {
      const url = `http://localhost:8080/api/appointments/${appointment}/cancel/`
      const fetchConfig = {
        method: "put",
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok){
        window.location.reload()
      }
    }


    useEffect(() => {fetchData()}, [])


    return(
        <div>
            <h1>Service Appointment</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {appointments.filter((appointment) => appointment.status.name === 'sub').map(appointment => {
              return (
                <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.isVIP ? "Yes" : "No"}</td>
                    <td>
                    {new Date(appointment.date_time).toLocaleDateString()}
                    </td>
                    <td>
                    {new Date(appointment.date_time).toLocaleTimeString()}
                    </td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td>
                      <button onClick={()=>handleFinish(appointment.id)} className="btn btn-primary me-md-4">Finish</button>
                      <button onClick={()=>handleCancel(appointment.id)} className="btn btn-primary me-md-4 btn-danger">Cancel</button>
                    </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    )
}
export default AppointmentList
