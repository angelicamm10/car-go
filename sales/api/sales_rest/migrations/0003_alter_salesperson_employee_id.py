# Generated by Django 4.0.3 on 2023-06-08 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_alter_sale_price_alter_salesperson_employee_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='employee_id',
            field=models.CharField(max_length=200),
        ),
    ]
