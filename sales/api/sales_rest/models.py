from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href= models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)

    def __str__(self):
        return (self.name)
    
    def get_api_url(self):
        return reverse("api_show_salespeople", kwargs={"pk": self.id})
    

class Customer(models.Model):
    first_name =models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return (self.first_name)
    
    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"pk": self.id})
