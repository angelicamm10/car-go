from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Appointment, AutomobileVO, Technician
from .encoders import (
    AppointmentListEncoder,
    AutomobileVOEncoder,
    TechnicianListEncoder)


@require_http_methods(["GET"])
def api_get_vin(request):
    try:
        auto = AutomobileVO.objects.all()
    except AutomobileVO.DoesNotExist:
        return JsonResponse(
            {"message": "Model not available"},
            status=404,
        )
    return JsonResponse(
        {"autos": auto},
        encoder=AutomobileVOEncoder,
    )


@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "No model"},
                status = 404,
            )
        return JsonResponse(
            { "technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status = 404,
            )
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_technician(request, pk):
    try:
        count, _ = Technician.objects.filter(id=pk).delete()
    except Technician.DoesNotExist:
        return JsonResponse(
            {"message": "No technician with that id"},
            status = 404,
        )
    return JsonResponse({"deleted": count > 0}, status = 200)


@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "No model"},
                status = 400,
            )
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentListEncoder,
            safe = False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status = 400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe = False
        )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, pk):
    try:
        count, _ = Appointment.objects.filter(id=pk).delete()
    except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "No Appointment exists"},
                status = 400,
            )
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Cannot finish appointment"},
                status=400,
            )
    appointment.finished()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Cannot cancel appointment"},
                status = 400,
            )
    appointment.cancelled()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )
